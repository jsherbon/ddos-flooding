import java.util.ArrayList;

import com.mysql.jdbc.Connection;

public class FindAttackIPs {
	
	public void test(int runID, ArrayList<String> TCPAttackIPs, ArrayList<String> UDPAttackIPs, ArrayList<String> ICMPAttackIPs, ArrayList<Attacks> attackInfo)
	{
		String protocol = "";

		
		for(int i = 0; i < 3; i++)
		{
			ArrayList<String> sourceIPs = new ArrayList<String>();

						
			if(i == 0)
			{
				protocol = "TCP";
				GetValidSourceIP(runID, protocol, sourceIPs);
				GetAttackIP(sourceIPs, protocol, runID, TCPAttackIPs, attackInfo);
				//System.out.println("TCP " + TCPAttackIPs);
			}
			else if(i == 1)
			{
				protocol = "UDP";
				GetValidSourceIP(runID, protocol, sourceIPs);
				GetAttackIP(sourceIPs, protocol, runID, UDPAttackIPs, attackInfo);
				//System.out.println("UDP " + UDPAttackIPs);
			}
			else
			{
				protocol = "ICMP";
				GetValidSourceIP(runID, protocol, sourceIPs);
				GetAttackIP(sourceIPs, protocol, runID, ICMPAttackIPs, attackInfo);
				//System.out.println("ICMP" + ICMPAttackIPs);
			}
			
		}
	}
	
	
	public void GetValidSourceIP(int runid, String proto, ArrayList<String> sourceIP)
	{
		
		SaveToDatabase save = new SaveToDatabase();
		
		sourceIP = save.GetSourceIP(save.EstablishConnection(), proto, runid, sourceIP);
		
	}
	
	public void GetAttackIP(ArrayList<String> sourceIP, String proto, int runid, ArrayList<String> attackIPs, ArrayList<Attacks> attackInfo)
	{
		SaveToDatabase save = new SaveToDatabase();
		java.sql.Connection conn = save.EstablishConnection();
		Long min = null;
		Long max = null;
		
		String srcIP = "";
		
		for(int i = 0; i < sourceIP.size(); i++)
		{
			ArrayList<Long> mMList = new ArrayList<Long>();
			srcIP = sourceIP.get(i);
			save.GetMinMax(conn, proto, runid, srcIP, mMList);
			min = mMList.get(0);
			max = mMList.get(1);
			while(min<max)
			{
				Attacks atck = new Attacks();
				save.GetAttack(atck, conn, proto, runid, srcIP, min, attackIPs);
				
				if(atck.getVictimIP() != null)
				{
					attackInfo.add(atck);
				}
				
				if((min+300000) > max )
				{
					min = max;
				}
				else
				{
					min = min + 300000;
				}
			}
		}
		
		save.CloseConnection(conn);
	}

}
