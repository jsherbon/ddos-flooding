
public class Packet {
	String destIP = "";
	String srcIP = "";
	String protocol = "";
	long timeStamp = 0;
	int runID = 0;
	
	//Initializes the object
	public void NewPacket(
			int runID,
			String destIP,
			String srcIP,
			String protocol,
			long timeStamp) {

			this.runID = runID;
			this.destIP = destIP;
			this.srcIP = srcIP;
			this.protocol = protocol;
			this.timeStamp = timeStamp;
			
			return;
	}
	
	

	@Override
	public String toString() {
		return destIP + "," + srcIP + "," + protocol + "," + timeStamp + "," + runID + "\n";
	}



	public String getDestIP() {
		return destIP;
	}

	public String getSrcIP() {
		return srcIP;
	}

	public String getProtocol() {
		return protocol;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public int getRunID() {
		return runID;
	}
}
	
	
