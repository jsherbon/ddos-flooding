import java.util.ArrayList;

public class Attacks {
	ArrayList<String> attackerIPs = new ArrayList<String>();
	String victimIP = null;
	String protocol = null;
	double packetsPerSecond = 0.0;
	
	public void NewAttack(
			ArrayList<String> attackerIPs,
			String victimIP,
			String protocol,
			double packetsPerSecond) {
			this.attackerIPs = attackerIPs;
			this.victimIP = victimIP;
			this.protocol = protocol;
			this.packetsPerSecond = packetsPerSecond;
		}
	

	@Override
	public String toString() {
		return "Attacks [attackerIPs=" + attackerIPs + ", victimIP=" + victimIP + ", protocol=" + protocol
				+ ", packetsPerSecond=" + packetsPerSecond + "]";
	}



	public ArrayList<String> getAttackerIPs() {
		return attackerIPs;
	}

	public String getVictimIP() {
		return victimIP;
	}

	public String getProtocol() {
		return protocol;
	}

	public double getPacketsPerSecond() {
		return packetsPerSecond;
	}
	

}
