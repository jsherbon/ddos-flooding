
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jnetpcap.ByteBufferHandler;
import org.jnetpcap.Pcap;
import org.jnetpcap.nio.JMemory;
import org.jnetpcap.packet.JFlow;
import org.jnetpcap.packet.JFlowKey;
import org.jnetpcap.packet.JFlowMap;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.JPacketHandler;
import org.jnetpcap.packet.JScanner;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.network.Icmp;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Http;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;
import org.jnetpcap.protocol.voip.Sip;

import com.mysql.jdbc.Connection;

public class ConvertPcapToPacket {
	boolean databaseUpdates = true;
	boolean packetCount = true;
	int packetInc = 100;

	int packetNumber = 0;
	int printNumber = 0;

	public int loopThroughFiles(ArrayList<String> fileNames, StringBuilder errbuf) {
		int runID = 0;
		for (int i = 0; i < fileNames.size(); i++) {
			String filePath = "";
			String newFilePath = "";
			// This is the new object to save in the database
			SaveToDatabase save = new SaveToDatabase();

			PcapngToPcap fileConversion = new PcapngToPcap();

			filePath = fileNames.get(i);
			if (filePath.matches(".*.pcapng")) {
				// System.out.println(filePath);
				newFilePath = filePath.substring(0, filePath.length() - 2);
				// System.out.println(newFilePath);
				try {
					fileConversion.convert(filePath, newFilePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
				runID = ConvertPcapToPacket(newFilePath, errbuf, save);
				return runID;
			} else {
				try {
					fileConversion.convert(filePath, filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
				runID = ConvertPcapToPacket(filePath, errbuf, save);
				return runID;
			}

		}
		return runID;
	}

	public int ConvertPcapToPacket(String filePath, StringBuilder errbuf, SaveToDatabase save) {
		int runID = 0;
		// JnetPCAP declarations for different header types
		Ip4 ip = new Ip4();
		Tcp tcp = new Tcp();
		Http http = new Http();
		Udp udp = new Udp();
		Icmp icmp = new Icmp();
		Sip sip = new Sip();
		
		// This is the new object for the packet
		Packet packet = new Packet();

		// opens the pcap file
		final Pcap pcap = Pcap.openOffline(filePath, errbuf);
		// prints an error if the file is empty or cannot be oppened
		if (pcap == null) {
			System.err.println(errbuf); // Error is stored in errbuf if any
			return 0;
		}
		if(databaseUpdates == true) {
		runID = save.RunID();
		}
		/*
		 * calls the preparepcap method. This will loop through the pcap file
		 * and convert it into objects and save it to the database
		 */
		PreparePcap(save.EstablishConnection(), pcap, errbuf, save, packet, runID, ip, tcp, http, udp, icmp, sip);
		
		return runID;
		

	}

	public void PreparePcap(java.sql.Connection connection, Pcap pcap, StringBuilder errbuf, SaveToDatabase save, Packet packet, int runID, Ip4 ip, Tcp tcp, Http http, Udp udp, Icmp icmp, Sip sip) {
		// opens loop that will go through the entire file passed
		pcap.loop(0, new JPacketHandler<StringBuilder>() {
			public void nextPacket(JPacket pp, StringBuilder errbuf) {

				
				
				// Declarations
				byte[] dIP = new byte[4]; // destination ip
				byte[] sIP = new byte[4]; // source ip
				boolean fAck = false; // Ack flag
				boolean fRst = false; // Rst flag
				int icmpType = 0; // Type value for ICMP protocol
				String protocol = ""; // protocol name
				long timeStamp; // timestamp in long form.

				// SECTION IS FOR ALL PACKETS
				// checks to see if there is an IP header
				if (pp.hasHeader(ip)) {
					// sets destination IP address as a byte
					dIP = pp.getHeader(ip).destination();
					// sets source IP address as a byte
					sIP = pp.getHeader(ip).source();
				} else {
					System.err.println("Couldnt find IP heading");
					return;
				}

				// formats source ip from a byte format to a string
				String sourceIP = org.jnetpcap.packet.format.FormatUtils.ip(sIP);
				// formats destination ip from a byte format to a string
				String destinationIP = org.jnetpcap.packet.format.FormatUtils.ip(dIP);

				// Gets timestamp information
				timeStamp = pp.getCaptureHeader().timestampInMillis();

				// SECTION TO GET PROTOCOL SPECIFIC INFORMATION

				try {
					// checks to see if packet is ICMP protocol
					if (pp.hasHeader(icmp)) {
						// gets ICMP type information
						icmpType = pp.getHeader(icmp).type();
						if(icmpType != 3 && icmpType != 0 && icmpType != 14 && icmpType != 16 && icmpType != 18)
						{
							return;
						}
						// sets protocol to ICMP
						protocol = "ICMP";
					}
					// checks to see if protocol is SIP
					else if (pp.hasHeader(sip)) {
						// returns to next packet since we are not interested in
						// data other than TCP, UDP, ICMP
						// System.out.println("Packet with invalid protocol");
						return;
					}
					// checks to see if protocol is TCP
					else if (pp.hasHeader(tcp)) {
						// gets Ack flag
						fAck = pp.getHeader(tcp).flags_ACK();
						// gets Rst flag
						fRst = pp.getHeader(tcp).flags_RST();
						// sets protocol to TCP
						protocol = "TCP";
						if ((fAck == false && fRst == false)) {
							return;
						}
					}
					// cehcks to see if protocol is UDP
					else if (pp.hasHeader(udp)) {
						// sets protocol to UDP
						protocol = "UDP";
					} else {
						// returns to next packet since we are not interested in
						// data other than TCP, UDP, ICMP
						// System.out.println("Packet with invalid protocol");
						return;
					}
				} catch (IndexOutOfBoundsException e) {
					return;
				}

				// Create new packet object
				packet.NewPacket(runID, destinationIP, sourceIP, protocol, timeStamp);
				if (packetCount == false) {
					// System.out.println(packet.toString());
				} else {
					if (packetNumber == printNumber) {
						// System.out.println(packetNumber + packet.toString());
						System.out.println(packetNumber);
						printNumber = printNumber + packetInc;
					}
					packetNumber++;
				}

				if (databaseUpdates == true) {
					// This calls the SaveToDatabase class to save the packet in
					// the database table
					save.InsertPacket(packet, connection);
				}

			}// end of next packet
			
			

		}, errbuf);// end of loop
		
		save.CloseConnection(connection);
	}
}
