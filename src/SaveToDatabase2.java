import java.io.BufferedWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;

import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class SaveToDatabase2 {
	
	public int RunID()
	{
		return GetRunID(EstablishConnection());
	}
	
	public int GetRunID(Connection conn)
	{
		int runID = 0;
		
		try {
			
			//creates the SP call string
			String callSP = "{call getRunID()}";
			
			//Creates a callable statment and begins the SP
			CallableStatement stmt=conn.prepareCall(callSP); 
			
			java.sql.ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				runID = rs.getInt(1);
			}
				
			
			//closes the connection with the database
			conn.close();
			
			//catches any exceptions and prints it the output
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return runID;
	}
	
	//This method takes the packet and converts it into the SP and excecutes the insert
	public void InsertPacket(ArrayList<Packet>packet, Connection conn )
	{
		try {
			int counter = 0;
			int segment = 0;
			while(counter<packet.size())
			{
				if((counter+25000) > packet.size())
				{
					segment = packet.size();
				}
				else
				{
					segment = counter + 25000;
				}
				// Create StringBuilder to String that will become stream
	            StringBuilder builder = new StringBuilder();
	            StringBuilder query = new StringBuilder();
	            
	            Packet pck = new Packet();
	            
	            for(counter=counter; counter < segment; counter++)
	            {
	            	pck = packet.get(counter);
	            	builder.append("('");
	            	builder.append(pck.getSrcIP());
	            	builder.append("','");
	            	builder.append(pck.getDestIP());
	            	builder.append("','");
	            	builder.append(pck.getProtocol());
	            	builder.append("',");
	            	builder.append(pck.getTimeStamp());
	            	builder.append(",");
	            	builder.append(pck.getRunID());
	            	builder.append(")");
	            	if((segment-counter) != 1)
	            	{
	            		builder.append(",");
	            	}
	            	
	            }
	            //creates the SP call string
				String callSP = "INSERT INTO ddos.packet_information (source_ip, destination_ip, protocol, wireshark_timestamp, run_id)	Values";
	            query.append(callSP);
	            query.append(builder);
	            query.append(";");
				
				//Creates a callable statment and begins the SP
				java.sql.Statement stmt=conn.createStatement(); 
				
				//executes the stored procedure
				stmt.executeUpdate(query.toString());
			}
			
			CloseConnection(conn);
			//catches any exceptions and prints it the output
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}

	}//end of insert packet
	
	public ArrayList<String> GetSourceIP(Connection conn, String proto, int runID, ArrayList<String> sourceIP)
	{
		int counter = 0;
		try {
			String callSP = "call findSourceIP(?,?)";
			
			//Creates a callable statment and begins the SP
			CallableStatement stmt=conn.prepareCall(callSP);
			
			stmt.setString(1, proto);
			stmt.setInt(2, runID);
			

			java.sql.ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				sourceIP.add(counter, rs.getString(1));
				counter ++;
			}
			
			conn.close();
			
			return sourceIP;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return null;
	}
	
	public void GetMinMax(Connection conn, String proto, int runID, String srcIP, ArrayList<Long> minMax)
	{
		try {
			
			//creates the SP call string
			String callSP = "{call getMinMAX(?,?,?)}";
			
			//Creates a callable statment and begins the SP
			CallableStatement stmt=conn.prepareCall(callSP); 
			
			stmt.setString(1, srcIP);
			stmt.setInt(2, runID);
			stmt.setString(3, proto);
			
			java.sql.ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				minMax.add(rs.getLong(1));
				minMax.add(rs.getLong(2));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return;
	}
	
	public void GetAttack(Attacks atck, Connection conn, String proto, int runID, String srcIP, Long startTime, ArrayList<String> attackIP)
	{
		try {
			
			String attackIPs = "";
			ArrayList<String> currentAttack = new ArrayList<String>();
						
			//creates the SP call string
			String callSP = "{call checkIfPeriodIsAttack(?,?,?,?)}";
			String callSP2 = "{call getAttackDetails(?,?,?,?)}";
			
			//Creates a callable statment and begins the SP
			CallableStatement stmt=conn.prepareCall(callSP); 
			
			stmt.setLong(1, startTime);
			stmt.setString(2, srcIP);
			stmt.setInt(3, runID);
			stmt.setString(4, proto);
			
			java.sql.ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				attackIPs = rs.getString(1);
				if(attackIPs.equals("packet failed"))
				{
					
				}
				else
				{
					attackIP.add(attackIPs);
					currentAttack.add(attackIPs);
				}
				
			}
			if(attackIPs.equals("packet failed")) {
			}
			else
			{
				stmt = conn.prepareCall(callSP2);
				stmt.setLong(1, startTime);
				stmt.setString(2, srcIP);
				stmt.setInt(3, runID);
				stmt.setString(4, proto);
				rs = stmt.executeQuery();
				rs.next();
				atck.NewAttack(currentAttack, srcIP, proto, rs.getDouble(1));
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return;
	}
	
	//This function creates the datasource
	public Connection EstablishConnection()
	{
		//creates new datasource
		MysqlDataSource dataSource = new MysqlDataSource();
		
		//enters user name
		dataSource.setUser("ddos_program");
		
		//enters password
		dataSource.setPassword("ddospassword");
		
		//enters server name
		dataSource.setServerName("127.0.0.1");
		
		//enter scheme
		dataSource.setDatabaseName("ddos");
		
		Connection connect;
		try {
			connect = dataSource.getConnection();
			return connect;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}//end of function
	
	public void CloseConnection(Connection conn)
	{
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void DeleteRun(int runID, Connection conn )
	{
		try {
	            //creates the SP call string
				String callSP = "Delete from ddos.packet_information where run_id =" + runID + ";";
				
				//Creates a callable statment and begins the SP
				java.sql.Statement stmt=conn.createStatement(); 
				
				//executes the stored procedure
				stmt.executeUpdate(callSP);
				
				CloseConnection(conn);
			//catches any exceptions and prints it the output
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}

	}//end of insert packet

}
