
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jnetpcap.ByteBufferHandler;
import org.jnetpcap.Pcap;
import org.jnetpcap.nio.JMemory;
import org.jnetpcap.packet.JFlow;
import org.jnetpcap.packet.JFlowKey;
import org.jnetpcap.packet.JFlowMap;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.JPacketHandler;
import org.jnetpcap.packet.JScanner;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.network.Icmp;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Http;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;
import org.jnetpcap.protocol.voip.Sip;

import com.mysql.jdbc.Connection;

public class ConvertPcapToPacket2 {
	boolean packetCount = true;
	int packetInc = 100000;

	int packetNumber = 0;
	int printNumber = 0;

	public int loopThroughFiles(int runID, ArrayList<String> fileNames, StringBuilder errbuf, ArrayList<Packet> tcpList, ArrayList<Packet> udpList, ArrayList<Packet> icmpList) {
		SaveToDatabase2 save = new SaveToDatabase2();
		runID = save.RunID();
		for (int i = 0; i < fileNames.size(); i++) {
			String filePath = "";
			String newFilePath = "";

			PcapngToPcap fileConversion = new PcapngToPcap();

			filePath = fileNames.get(i);
			if (filePath.matches(".*.pcapng")) {
				// System.out.println(filePath);
				newFilePath = filePath.substring(0, filePath.length() - 2);
				// System.out.println(newFilePath);
				try {
					fileConversion.convert(filePath, newFilePath);
				} catch (IOException e) {
					System.out.println("File Not Found");
				}
				ConvertPcapToPacket(runID, save, newFilePath, errbuf, tcpList, udpList,icmpList);
			} else {
				try {
					fileConversion.convert(filePath, filePath);
				} catch (IOException e) {
					System.out.println("File Not Found");
				}
				ConvertPcapToPacket(runID, save, filePath, errbuf, tcpList, udpList,icmpList);
			}

		}
		return runID;
	}

	public void ConvertPcapToPacket(int runID, SaveToDatabase2 save, String filePath, StringBuilder errbuf, ArrayList<Packet> tcpList, ArrayList<Packet> udpList, ArrayList<Packet> icmpList) {
		// JnetPCAP declarations for different header types
		Ip4 ip = new Ip4();
		Tcp tcp = new Tcp();
		Http http = new Http();
		Udp udp = new Udp();
		Icmp icmp = new Icmp();
		Sip sip = new Sip();		
		

		// opens the pcap file
		final Pcap pcap = Pcap.openOffline(filePath, errbuf);
		// prints an error if the file is empty or cannot be oppened
		if (pcap == null) {
			System.err.println(errbuf); // Error is stored in errbuf if any
			return;
		}
		
		/*
		 * calls the preparepcap method. This will loop through the pcap file
		 * and convert it into objects and save it to the database
		 */
		PreparePcap(runID, tcpList, udpList, icmpList, pcap, errbuf, ip, tcp, http, udp, icmp, sip);
		
		save.InsertPacket(tcpList, save.EstablishConnection());
		tcpList.clear();
		System.out.println("TCP Complete");
		save.InsertPacket(udpList, save.EstablishConnection());
		udpList.clear();
		System.out.println("UDP Complete");
		save.InsertPacket(icmpList, save.EstablishConnection());
		icmpList.clear();
		System.out.println("ICMP Complete");
		
		//System.out.println(icmpList);
		

	}

	public void PreparePcap(int runID, ArrayList<Packet> tcpList, ArrayList<Packet> udpList, ArrayList<Packet> icmpList, Pcap pcap, StringBuilder errbuf, Ip4 ip, Tcp tcp, Http http, Udp udp, Icmp icmp, Sip sip) {
		// opens loop that will go through the entire file passed
		pcap.loop(0, new JPacketHandler<StringBuilder>() {
			public void nextPacket(JPacket pp, StringBuilder errbuf) {
				
				// This is the new object for the packet
				Packet packet = new Packet();
				
				// Declarations
				byte[] dIP = new byte[4]; // destination ip
				byte[] sIP = new byte[4]; // source ip
				boolean fAck = false; // Ack flag
				boolean fRst = false; // Rst flag
				int icmpType = 0; // Type value for ICMP protocol
				long timeStamp; // timestamp in long form.

				// SECTION IS FOR ALL PACKETS
				// checks to see if there is an IP header
				if (pp.hasHeader(ip)) {
					// sets destination IP address as a byte
					dIP = pp.getHeader(ip).destination();
					// sets source IP address as a byte
					sIP = pp.getHeader(ip).source();
				} else {
					System.err.println("Couldnt find IP heading");
					return;
				}

				// formats source ip from a byte format to a string
				String sourceIP = org.jnetpcap.packet.format.FormatUtils.ip(sIP);
				// formats destination ip from a byte format to a string
				String destinationIP = org.jnetpcap.packet.format.FormatUtils.ip(dIP);

				// Gets timestamp information
				timeStamp = pp.getCaptureHeader().timestampInMillis();

				// SECTION TO GET PROTOCOL SPECIFIC INFORMATION

				try {
					// checks to see if packet is ICMP protocol
					if (pp.hasHeader(icmp)) {
						// gets ICMP type information
						icmpType = pp.getHeader(icmp).type();
						if(icmpType != 3 && icmpType != 0 && icmpType != 14 && icmpType != 16 && icmpType != 18)
						{
							return;
						}
						// Create new packet object
						packet.NewPacket(runID, destinationIP, sourceIP, "ICMP", timeStamp);
						icmpList.add(packet);
					}
					// checks to see if protocol is SIP
					else if (pp.hasHeader(sip)) {
						// returns to next packet since we are not interested in
						// data other than TCP, UDP, ICMP
						// System.out.println("Packet with invalid protocol");
						return;
					}
					// checks to see if protocol is TCP
					else if (pp.hasHeader(tcp)) {
						// gets Ack flag
						fAck = pp.getHeader(tcp).flags_ACK();
						// gets Rst flag
						fRst = pp.getHeader(tcp).flags_RST();
						if ((fAck == false && fRst == false)) {
							return;
						}
						packet.NewPacket(runID, destinationIP, sourceIP, "TCP", timeStamp);
						tcpList.add(packet);
					}
					// cehcks to see if protocol is UDP
					else if (pp.hasHeader(udp)) {
						packet.NewPacket(runID, destinationIP, sourceIP, "UDP", timeStamp);
						udpList.add(packet);
					} else {
						// returns to next packet since we are not interested in
						// data other than TCP, UDP, ICMP
						// System.out.println("Packet with invalid protocol");
						return;
					}
				} catch (IndexOutOfBoundsException e) {
					return;
				}

				
				if (packetCount == false) {
					// System.out.println(packet.toString());
				} else {
					if (packetNumber == printNumber) {
						// System.out.println(packetNumber + packet.toString());
						System.out.println(packetNumber);
						printNumber = printNumber + packetInc;
					}
					packetNumber++;
				}

			}// end of next packet
			
			

		}, errbuf);// end of loop
	}
}
