
public class PacketUpdated {
	String destIP = "";
	String srcIP = "";
	long timeStamp = 0;
	
	//Initializes the object
	public void NewPacket(
			String destIP,
			String srcIP,
			long timeStamp) {

			this.destIP = destIP;
			this.srcIP = srcIP;
			this.timeStamp = timeStamp;
						
			return;
	}
	
	

	@Override
	public String toString() {
		return "Packet [destIP=" + destIP + ", srcIP=" + srcIP + ", timeStamp=" + timeStamp
				+ "]";
	}



	public String getDestIP() {
		return destIP;
	}

	public String getSrcIP() {
		return srcIP;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

}
	
	
