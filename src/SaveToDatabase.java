import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class SaveToDatabase {
	
	public int RunID()
	{
		return GetRunID(EstablishConnection());
	}
	
	public int GetRunID(Connection conn)
	{
		int runID = 0;
		
		try {
			
			//creates the SP call string
			String callSP = "{call getRunID()}";
			
			//Creates a callable statment and begins the SP
			CallableStatement stmt=conn.prepareCall(callSP); 
			
			java.sql.ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				runID = rs.getInt(1);
			}
				
			
			//closes the connection with the database
			conn.close();
			
			//catches any exceptions and prints it the output
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return runID;
	}
	
	//This method takes the packet and converts it into the SP and excecutes the insert
	public void InsertPacket(Packet packet, Connection conn )
	{
		try {
			
			//creates the SP call string
			String callSP = "{call insert_packet(?,?,?,?,?)}";
			
			//Creates a callable statment and begins the SP
			CallableStatement stmt=conn.prepareCall(callSP); 
			
			//inserts the information into the stored procedures
			stmt.setString(1, packet.getDestIP());
			stmt.setString(2, packet.getSrcIP());
			stmt.setString(3, packet.getProtocol());
			stmt.setLong(4, packet.getTimeStamp());
			stmt.setInt(5, packet.getRunID());
			
			//executes the stored procedure
			stmt.executeQuery();
			
			//catches any exceptions and prints it the output
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}//end of insert packet
	
	public ArrayList<String> GetSourceIP(Connection conn, String proto, int runID, ArrayList<String> sourceIP)
	{
		int counter = 0;
		try {
			String callSP = "call findSourceIP(?,?)";
			
			//Creates a callable statment and begins the SP
			CallableStatement stmt=conn.prepareCall(callSP);
			
			stmt.setString(1, proto);
			stmt.setInt(2, runID);
			

			java.sql.ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				sourceIP.add(counter, rs.getString(1));
				counter ++;
			}
			
			conn.close();
			
			return sourceIP;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return null;
	}
	
	public void GetMinMax(Connection conn, String proto, int runID, String srcIP, ArrayList<Long> minMax)
	{
		try {
			
			//creates the SP call string
			String callSP = "{call getMinMAX(?,?,?)}";
			
			//Creates a callable statment and begins the SP
			CallableStatement stmt=conn.prepareCall(callSP); 
			
			stmt.setString(1, srcIP);
			stmt.setInt(2, runID);
			stmt.setString(3, proto);
			
			java.sql.ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				minMax.add(rs.getLong(1));
				minMax.add(rs.getLong(2));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return;
	}
	
	public void GetAttack(Attacks atck, Connection conn, String proto, int runID, String srcIP, Long startTime, ArrayList<String> attackIP)
	{
		try {
			
			String attackIPs = "";
			ArrayList<String> currentAttack = new ArrayList<String>();
						
			//creates the SP call string
			String callSP = "{call checkIfPeriodIsAttack(?,?,?,?)}";
			String callSP2 = "{call getAttackDetails(?,?,?,?)}";
			
			//Creates a callable statment and begins the SP
			CallableStatement stmt=conn.prepareCall(callSP); 
			
			stmt.setLong(1, startTime);
			stmt.setString(2, srcIP);
			stmt.setInt(3, runID);
			stmt.setString(4, proto);
			
			java.sql.ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				attackIPs = rs.getString(1);
				if(attackIPs.equals("packet failed"))
				{
					
				}
				else
				{
					attackIP.add(attackIPs);
					currentAttack.add(attackIPs);
				}
				
			}
			if(attackIPs.equals("packet failed")) {
			}
			else
			{
				stmt = conn.prepareCall(callSP2);
				stmt.setLong(1, startTime);
				stmt.setString(2, srcIP);
				stmt.setInt(3, runID);
				stmt.setString(4, proto);
				rs = stmt.executeQuery();
				rs.next();
				atck.NewAttack(currentAttack, srcIP, proto, rs.getDouble(1));
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return;
	}
	
	//This function creates the datasource
	public Connection EstablishConnection()
	{
		//creates new datasource
		MysqlDataSource dataSource = new MysqlDataSource();
		
		//enters user name
		dataSource.setUser("ddos_program");
		
		//enters password
		dataSource.setPassword("ddospassword");
		
		//enters server name
		dataSource.setServerName("127.0.0.1");
		
		//enter scheme
		dataSource.setDatabaseName("ddos");
		
		Connection connect;
		try {
			connect = dataSource.getConnection();
			return connect;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}//end of function
	
	public void CloseConnection(Connection conn)
	{
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
