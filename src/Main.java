import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.jnetpcap.Pcap;  
import org.jnetpcap.nio.JMemory;  
import org.jnetpcap.packet.JFlow;  
import org.jnetpcap.packet.JFlowKey;  
import org.jnetpcap.packet.JFlowMap;  
import org.jnetpcap.packet.JPacket;  
import org.jnetpcap.packet.JPacketHandler;  
import org.jnetpcap.packet.JScanner;  
import org.jnetpcap.packet.PcapPacket;  
import org.jnetpcap.protocol.tcpip.Http;  
import org.jnetpcap.protocol.tcpip.Tcp;
import javax.swing.JOptionPane;


public class Main {

	public static void main(String[] args) {
		
		boolean database = true;
		SaveToDatabase2 db = new SaveToDatabase2();
		int runID = 0;
		
		//FRONT GUI SITS
		ArrayList<String> files = new ArrayList();
		ArrayList<String> TCPAttackIPs = new ArrayList<String>();
		ArrayList<String> UDPAttackIPs = new ArrayList<String>();
		ArrayList<String> ICMPAttackIPs = new ArrayList<String>();
		
		ArrayList<Packet> tcpList = new ArrayList<Packet>();
		ArrayList<Packet> udpList = new ArrayList<Packet>();
		ArrayList<Packet> icmpList = new ArrayList<Packet>();
		
		ArrayList<Attacks> AttackInformation = new ArrayList<Attacks>();
		
		final StringBuilder errbuf = new StringBuilder();
		
		System.loadLibrary("jnetpcap");		
		
		File folder = new File("C:" + File.separator + "Users" + File.separator + "jsher" + File.separator + "Documents" + File.separator + "DDOS Files");
		File[] listOfFiles = folder.listFiles();		
		for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		        files.add("C:" + File.separator + "Users" + File.separator + "jsher" + File.separator + "Documents" + File.separator + "DDOS Files" + File.separator + listOfFiles[i].getName());
		    }
		}
		
		ConvertPcapToPacket2 convert = new ConvertPcapToPacket2();
		runID = convert.loopThroughFiles(runID, files, errbuf, tcpList, udpList, icmpList);
		
		infoBox("Analysis in process", "Notification");
		
		FindAttackIPs attack = new FindAttackIPs();
		attack.test(runID, TCPAttackIPs, UDPAttackIPs, ICMPAttackIPs, AttackInformation);
		infoBox("Analysis Complete", "Notification");
		
		System.out.println(System.currentTimeMillis());
		
		System.out.println("Cleaning DB");
		db.DeleteRun(runID, db.EstablishConnection());
		System.out.println("Cleaning Complete");
		infoBox("Cleaning Complete", "Notification");
		
		//--------------------------------------------------------------------------------------------
		//Section To Be Replaced By Front End GUI and Export
		//All information should be in the arrays:
		//TCPAttackIPs, UDPAttackIPs, ICMPAttackIPs, AttackInformation
		//--------------------------------------------------------------------------------------------
		
		
	}
	
	
	public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
}
